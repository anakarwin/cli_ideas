from collections import deque
from random import randint


class Tile:
    MINE = 'b'
    CLOSED = ' '    
    FLAG = 'p'
    MARK = '?'
    BLANK = '0'
    NUMBER = '12345678'



class Board(object):
    """Represent board of mine game
    """
    def __init__(self, shape: tuple, percent_mines: int):
        self.shape = shape
        self.percent_mines = percent_mines        
        self._validate_input()
        self._generate_tiles()
        self._generate_visited()
        self._generate_mine_position()
        self._fill_with_number()
    
    def _validate_input(self):
        h, w = self.shape
        size = h * w
        assert 1 <= self.percent_mines <= 100, (
            'Percent mines should be between 1 and 100 inclusive'
        )
        self.mines = self.percent_mines * size // 100
        self.flags = self.mines
    
    def _generate_tiles(self):
        self.tiles = []
        h, w = self.shape
        for i in range(h):
            self.tiles.append([Tile.CLOSED] * w)
    
    def _generate_visited(self):
        self.visited = []
        h, w = self.shape
        for i in range(h):
            self.visited.append([False] * w)
    
    def _generate_mine_position(self):
        self.mine_pos = []
        h, w = self.shape
        for i in range(h):
            self.mine_pos.append([Tile.BLANK] * w)
        i = 0
        while True:            
            x = randint(0, w - 1)
            y = randint(0, h - 1)
            if self.mine_pos[y][x] != Tile.MINE:
                self.mine_pos[y][x] = Tile.MINE
                i += 1
            if i == self.mines:
                break
    
    def _fill_with_number(self):
        h, w = self.shape
        for y in range(h):
            for x in range(w):
                if self.mine_pos[y][x] != Tile.MINE:
                    n_mine = 0
                    for dx, dy in self._gen_neighbor(x, y):
                        if self.mine_pos[dy][dx] == Tile.MINE:
                            n_mine += 1
                    if n_mine == 0:
                        self.mine_pos[y][x] == Tile.BLANK
                    else:
                        self.mine_pos[y][x] = str(n_mine)
    
    def draw(self):
        """Draw board
        """
        h, w = self.shape
        # print first line
        print('-' * (4 * w + 1))
        for y in range(h):
            row = ' | '.join(self.tiles[y])
            print('| %s |' % row)
            print('-' * (4 * w + 1))

    def draw_mine_pos(self):
        h, w = self.shape
        # print first line
        print('-' * (4 * w + 1))
        for y in range(h):
            row = ' | '.join(self.mine_pos[y])
            print('| %s |' % row)
            print('-' * (4 * w + 1))
    
    def draw_visited(self):
        h, w = self.shape
        # print first line
        print('-' * (4 * w + 1))
        for y in range(h):
            row = ' | '.join(map(
                lambda x: 'T' if x else 'F',
                self.visited[y]
            ))
            print('| %s |' % row)
            print('-' * (4 * w + 1))
    
    def _gen_neighbor(self, x, y):
        h, w = self.shape
        for dx in [-1, 0, 1]:
            for dy in [-1, 0, 1]:
                if not(dx == 0 and dy == 0):
                    rx = x + dx
                    ry = y + dy
                    if (0 <= rx < w and 0 <= ry < h and
                            not self.visited[ry][rx]):
                        yield (rx, ry)

    def _reveal_all_mines(self):
        h, w = self.shape
        for y in range(h):
            for x in range(w):
                if self.mine_pos[y][x] == Tile.MINE:
                    self.tiles[y][x] = Tile.MINE
    
    def is_solved(self):
        h, w = self.shape
        for y in range(h):
            for x in range(w):
                if not self.visited[y][x] and self.mine_pos != Tile.MINE:
                    return False
        return True

    def open(self, sx, sy) -> bool:
        """Open tile on position x y
        """
        queue = deque([(sx, sy)])
        safe = True
        while queue:
            x, y = queue.popleft()
            self.visited[y][x] = True
            val = self.mine_pos[y][x]
            if val == Tile.BLANK:                
                self.tiles[y][x] = Tile.BLANK
                for dx, dy in self._gen_neighbor(x, y):
                    queue.append((dx, dy))
            elif val == Tile.MINE:
                # game over, reveal all mines
                safe = False
                self._reveal_all_mines()
            else:   # number tiles
                self.tiles[y][x] = val
        return safe
    
    def flag(self, x, y):
        """Mark tile with flag
        """
        if not self.visited[y][x] and self.flags:
            self.flags -= 1
            self.tiles[y][x] = Tile.FLAG
            self.visited[y][x] = True
    
    def mark(self, x, y):
        """Mark tile with mark
        """
        if not self.visited[y][x]:
            self.tiles[y][x] = Tile.MARK
            self.visited[y][x] = True
