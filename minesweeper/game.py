from board import Board


class Game(object):
    """Mine game engine
    """
    def __init__(self, board_shape: tuple, percent_mines: int, debug: bool):
        self.board = Board(board_shape, percent_mines)
        self.debug = debug
    
    def render(self):
        print('=' * (4 * self.board.shape[1] + 1))
        self.board.draw()
        print('Flags: %d' % self.board.flags)
        if self.debug:
            print('[debug] mine_pos')
            self.board.draw_mine_pos()
        print('>>>', end=' ')

    def run(self):
        self.play = True
        self.render()
        while self.play:
            line = input().strip()
            self._parse_command(line)
            self.render()
            if self.board.is_solved():
                self.play = False
        self.render()
        if self.board.is_solved():
            print("You're WON")
        else:
            print("You're LOSE")
    
    def _validate_pos(self, x, y):
        h, w = self.board.shape
        assert 0 <= x < w and 0 <= y < h
    
    def _parse_command(self, line):
        cmd, _, arg = line.partition(' ')
        if cmd == 'quit':
            self.play = False
        else:
            try:
                y, x = map(int, arg.split())
            except Exception:
                return
            self._validate_pos(x, y)
            if cmd == 'open':
                self.play = self.board.open(x, y)
            elif cmd == 'mark':
                self.board.mark(x, y)
            elif cmd == 'flag':
                self.board.flag(x, y)
