import argparse

from game import Game


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Console Mine game'
    )
    parser.add_argument(
        'height', type=int, help='Board height'
    )
    parser.add_argument(
        'width', type=int, help='Board width'
    )
    parser.add_argument(
        'mines_percent', type=int,
        help='Mines percentage of board'
    )
    parser.add_argument(
        '-d', '--debug', action='store_true',
        help='Debug mode'
    )
    args = parser.parse_args()
    g = Game((args.height, args.width), args.mines_percent, args.debug)
    g.run()
